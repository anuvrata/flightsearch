package com.ixigo.anuvrata.searchflight;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

//Launcher activity
public class MainActivity extends AppCompatActivity {
    private Spinner mSpinnerOrigin = null;
    private Spinner mSpinnerDestination = null;
    private Button mSearchButton = null;

    private final AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        //We want button to search be enabled only when origin and destination are seperate cities
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            String originCity = mSpinnerOrigin.getSelectedItem().toString();
            String destinationCity =mSpinnerDestination.getSelectedItem().toString();
            if(!originCity.equals(destinationCity))
            {
                mSearchButton.setEnabled(true);
            }
            else
            {
                mSearchButton.setEnabled(false);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSpinnerOrigin = (Spinner)findViewById(R.id.origin);
        mSpinnerDestination = (Spinner)findViewById(R.id.destination);
        mSearchButton = (Button)findViewById(R.id.search);

        mSearchButton.setEnabled(false);

        ArrayAdapter arrayAdapter = new ArrayAdapter (this, android.R.layout.simple_list_item_1, CityArray.sCityArray);
        mSpinnerOrigin.setAdapter(arrayAdapter);
        mSpinnerDestination.setAdapter(arrayAdapter);

        if(savedInstanceState!=null)
        {
            //Restoring state
            mSpinnerOrigin.setSelection(savedInstanceState.getInt("origin",0));
            mSpinnerDestination.setSelection((savedInstanceState.getInt("destination", 0)));
            mSearchButton.setEnabled(savedInstanceState.getBoolean("isButtonEnabled"));
        }

        mSpinnerOrigin.setOnItemSelectedListener(onItemSelectedListener);
        mSpinnerDestination.setOnItemSelectedListener(onItemSelectedListener);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ShowFlightDetails.class);
                intent.putExtra("Origin", mSpinnerOrigin.getSelectedItem().toString());
                intent.putExtra("Destination", mSpinnerDestination.getSelectedItem().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Saving spinner selection and button state when configuration change etc lead to activity destoy
        outState.putInt("origin", mSpinnerOrigin.getSelectedItemPosition());
        outState.putInt("destination", mSpinnerDestination.getSelectedItemPosition());
        outState.putBoolean("isButtonEnabled", mSearchButton.isEnabled());
    }
}
