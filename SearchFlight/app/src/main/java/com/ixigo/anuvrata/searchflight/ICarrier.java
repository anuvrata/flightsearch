package com.ixigo.anuvrata.searchflight;

//Interface to be implemented by any carriers like Flights, Railways, Buses etc.
public interface ICarrier {

    String getOrigin();

    String getDestination();

    String getArrivalTime();

    String getDepartureTime();

    String getFare();

    String getProvider();

    String getAirline();

    String getTravelClass();
}
