package com.ixigo.anuvrata.searchflight;

import java.util.ArrayList;

//Base task to fetch ICarrier list from server
public abstract class BaseTask implements CustomCallable {
    @Override
    public void setUiForLoading() {

    }

    @Override
    public void setDataAfterLoading(ArrayList<ICarrier> result) {

    }

    @Override
    public ArrayList<ICarrier> call() throws Exception {
        return null;
    }
}
