package com.ixigo.anuvrata.searchflight;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

//Custom adapter to show flight details
public class ListViewAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<ICarrier> mCarriers;

    ListViewAdapter(Context context, ArrayList<ICarrier> carriers)
    {
        super();
        mCarriers = carriers;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mCarriers.size();
    }

    @Override
    public Object getItem(int i) {
        return mCarriers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        TextView providerText;
        TextView arrivalTimeText;
        TextView departureTimeText;
        TextView fareText;

        public ViewHolder(View view) {
            providerText = (TextView)view.findViewById(R.id.providerText);
            arrivalTimeText = (TextView) view.findViewById(R.id.arrivalTimeText);
            departureTimeText = (TextView) view.findViewById(R.id.departureTimeText);
            fareText = (TextView) view.findViewById(R.id.fareText);
        }
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = LayoutInflater.from(mContext).inflate(R.layout.list_view_row, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.providerText.setText((mCarriers.get(i).getProvider()));
        viewHolder.arrivalTimeText.setText(mCarriers.get(i).getArrivalTime());
        viewHolder.departureTimeText.setText(mCarriers.get(i).getDepartureTime());
        viewHolder.fareText.setText("Rs. "+ mCarriers.get(i).getFare());

        return view;
    }
}
