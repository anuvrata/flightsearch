package com.ixigo.anuvrata.searchflight;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

//Activity showing flight details
public class ShowFlightDetails extends AppCompatActivity implements IHandleData {
    private LinearLayout mLinearLayoutForProgessBar = null;
    private TextView mNoFlightDataTetView = null;
    private ListView mListView = null;
    private Button mSortAscFare = null;
    private Button mSortAscArrivalTime = null;
    private Button mSortAscDepartureTime = null;
    private boolean[] mIsSortedBy = {false, false, false}; // boolean showing if list is sorted:
    // 0th index for by fare, 1st for by arrival time, 2nd for by departure time
    private ListViewAdapter mListAdapter = null;
    private ArrayList<ICarrier> mCarriers = null;
    private final String fareSortButtonTagName = "sortByFare";
    private final String arrivalTimeSortButtonTagName = "sortByArrivalTime";
    private final String departureSortButtonTagName = "sortByDepartureTime";


    private void setIsSortedBy(int i)
    {
        for(int j=0;j<3;j++)
        {
            mIsSortedBy[j]=(j==i)?true:false;
        }
    }

    //method to sort carriers
    private void sortCarriers(Comparator<ICarrier> carrierComparator, int sortBy /*to set boolean
    isSortedBy*/)
    {
        Collections.sort(mCarriers, carrierComparator);
        setIsSortedBy(sortBy);
    }

    //method to sort by fare
    private void sortByFare()
    {
        Comparator<ICarrier> carrierComparator = new Comparator<ICarrier>() {
            @Override
            public int compare(ICarrier userData, ICarrier t1) {
                Long fare1 = new Long(userData.getFare());
                Long fare2 = new Long(t1.getFare());
                return fare1.compareTo(fare2);
            }
        };
        sortCarriers(carrierComparator, 0);
    }

    //method to sort by arrival time
    private void sortByArrivalTime()
    {
        Comparator<ICarrier> carrierComparator = new Comparator<ICarrier>() {
            @Override
            public int compare(ICarrier userData, ICarrier t1) {
                Date arrivalTime1 = new Date(userData.getArrivalTime());
                Date arrivalTime2 = new Date(t1.getArrivalTime());
                return arrivalTime1.compareTo(arrivalTime2);
            }
        };
        sortCarriers(carrierComparator, 1);
    }

    //method to sort by departure time
    private void sortByDepartureTime()
    {
        Comparator<ICarrier> carrierComparator = new Comparator<ICarrier>() {
            @Override
            public int compare(ICarrier userData, ICarrier t1) {
                Date departureTime1 = new Date(userData.getDepartureTime());
                Date departureTime2 = new Date(t1.getDepartureTime());
                return departureTime1.compareTo(departureTime2);
            }
        };
        sortCarriers(carrierComparator, 2);
    }

    private View.OnClickListener onClickListenerForSorting = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getTag().equals(fareSortButtonTagName)) {
                sortByFare();
            }
            if(view.getTag().equals(arrivalTimeSortButtonTagName)) {
                sortByArrivalTime();
            }
            if(view.getTag().equals(departureSortButtonTagName)) {
                sortByDepartureTime();
            }
            mListAdapter = new ListViewAdapter (getBaseContext(), mCarriers);
            mListView.setAdapter(mListAdapter);
            mListAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_flight_details);

        if(savedInstanceState!=null)
        {
            mIsSortedBy=savedInstanceState.getBooleanArray("isSortedBy");
        }

        mListView = (ListView) findViewById(R.id.flightlistview);
        //Starting new activity to show details of a specific flight clicked in list view
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ICarrier carrier = (ICarrier) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(getBaseContext(), ShowFlightFullInfo.class);
                intent.putExtra("origin", carrier.getOrigin());
                intent.putExtra("destination", carrier.getDestination());
                intent.putExtra("departureTime",  carrier.getDepartureTime());
                intent.putExtra("arrivalTime", carrier.getArrivalTime());
                intent.putExtra("provider", carrier.getProvider());
                intent.putExtra("fare", carrier.getFare());
                intent.putExtra("airline", carrier.getAirline());
                intent.putExtra("travelClass", carrier.getTravelClass());

                startActivity(intent);
            }
        });

        mLinearLayoutForProgessBar = (LinearLayout) findViewById(R.id.linlaHeaderProgress);

        mNoFlightDataTetView = (TextView) findViewById(R.id.noDataTextView);

        mSortAscArrivalTime = (Button) findViewById(R.id.sortAscArrivalTime);
        mSortAscDepartureTime = (Button) findViewById(R.id.sortAscDepartureTime);
        mSortAscFare = (Button) findViewById(R.id.sortAscFare);

        mSortAscFare.setTag(fareSortButtonTagName);
        mSortAscDepartureTime.setTag(departureSortButtonTagName);
        mSortAscArrivalTime.setTag(arrivalTimeSortButtonTagName);

        mSortAscFare.setOnClickListener(onClickListenerForSorting);
        mSortAscArrivalTime.setOnClickListener(onClickListenerForSorting);
        mSortAscDepartureTime.setOnClickListener(onClickListenerForSorting);

        Intent intent = getIntent();
        //Origin selected by user
        String origin = intent.getStringExtra("Origin");
        //Destination selected by user
        String destination = intent.getStringExtra("Destination");

        TaskRunner runner = new TaskRunner();
        //Background task to fetch flight data for selected origin and destination
        runner.executeAsync(new GetFlightDataTask(this, origin, destination));
    }

    @Override
    public void showProgressBar() {
        mLinearLayoutForProgessBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mLinearLayoutForProgessBar.setVisibility(View.GONE);
    }

    //callback received when GetFlightDataTask has fetched ICarrier list
    @Override
    public void setDataInPageWithResult(ArrayList<ICarrier> result) {
        mCarriers = result;
        //If no data exists, we want to tell the users that no flight for selected route exists
        if(mCarriers.isEmpty())
        {
            mNoFlightDataTetView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            mSortAscArrivalTime.setEnabled(false);
            mSortAscFare.setEnabled(false);
            mSortAscDepartureTime.setEnabled(false);
            for(boolean sort : mIsSortedBy) sort = false;
        }
        else {
            //checking if data is fetched due to activity destroy (config change etc)
            //if true then we want to restore earlier sort selection
            for (int i = 0; i < 3; i++) {
                if (mIsSortedBy[i]) {
                    if (i == 0) sortByFare();
                    else if (i == 1) sortByArrivalTime();
                    else sortByDepartureTime();
                    break;
                }
            }
            //setting adapter with fetched flight data for showing in the list
            mListAdapter = new ListViewAdapter(getBaseContext(), mCarriers);
            mListView.setAdapter(mListAdapter);
            mListView.setVisibility(View.VISIBLE);
            mNoFlightDataTetView.setVisibility(View.GONE);
            mSortAscArrivalTime.setEnabled(true);
            mSortAscFare.setEnabled(true);
            mSortAscDepartureTime.setEnabled(true);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //Saving state
        super.onSaveInstanceState(outState);
        outState.putBooleanArray("isSortedBy", mIsSortedBy);
    }
}