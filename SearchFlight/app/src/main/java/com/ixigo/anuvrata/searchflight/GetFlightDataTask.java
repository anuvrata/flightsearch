package com.ixigo.anuvrata.searchflight;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

//Task that extends BaseTask and specializes in retrieving flight data
public class GetFlightDataTask extends BaseTask {
    private final IHandleData listener;//listener in activity
    private final String mOrigin;
    private final String mDestination;
    private final String mUrl = "http://www.mocky.io/v2/5979c6731100001e039edcb3"; //URL for flight
    // data

    //method to convert epoch time to String IST Date Time
    private String getDateTimeIST(Long milliseconds)
    {
        Date date = new Date(milliseconds);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        format.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return format.format(date);
    }

    public GetFlightDataTask(IHandleData onDataFetchedListener, String origin, String destination) {
        this.listener = onDataFetchedListener;
        mOrigin = origin;
        mDestination = destination;
    }

    //method to fetch JSON Flight details for URL and populate list of Flights
    @Override
    public ArrayList<ICarrier> call() throws Exception {
        ArrayList<ICarrier> result = new ArrayList<>();
        HttpURLConnection urlConnection = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            URL url = new URL(mUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }

        }catch( Exception e) {
            e.printStackTrace();
        }
        finally {
            if(urlConnection!=null)
            {
                urlConnection.disconnect();
            }
        }
        JSONObject jsonObject = new JSONObject(stringBuilder.toString());
        JSONArray flightsArray = jsonObject.getJSONArray("flights");
        JSONObject appendixObject = jsonObject.getJSONObject("appendix");
        for(int i=0; i<flightsArray.length(); i++)
        {
            JSONObject flightsArrayJSONObject = flightsArray.getJSONObject(i);
            String origin = appendixObject.getJSONObject("airports").getString(flightsArrayJSONObject.getString("originCode"));
            String destination = appendixObject.getJSONObject("airports").getString(flightsArrayJSONObject.getString("destinationCode"));

            //Checking if origin and destination matches with the user input
            if(!origin.equalsIgnoreCase(mOrigin) || !destination.equalsIgnoreCase(mDestination))
            {
                continue;
            }

            String departureTime = getDateTimeIST(flightsArrayJSONObject.getLong("departureTime"));
            String arrivalTime = getDateTimeIST(flightsArrayJSONObject.getLong("arrivalTime"));
            String airline = appendixObject.getJSONObject("airlines").getString(flightsArrayJSONObject.getString("airlineCode"));
            String travelClass = flightsArrayJSONObject.getString("class");

            JSONArray fares = flightsArrayJSONObject.getJSONArray("fares");
            for(int j=0;j<fares.length();j++) {
                JSONObject faresJSONObject = fares.getJSONObject(j);
                String provider = appendixObject.getJSONObject("providers").getString(faresJSONObject.getString("providerId"));
                String fare = faresJSONObject.getString("fare");

                ICarrier flightObject = new Flights(origin, destination, arrivalTime,
                        departureTime, fare, provider, airline, travelClass);

                result.add(flightObject);
            }
        }
        return result;
    }

    @Override
    public void setUiForLoading() {
        listener.showProgressBar();
    }

    @Override
    public void setDataAfterLoading(ArrayList<ICarrier> result) {
        listener.setDataInPageWithResult(result);
        listener.hideProgressBar();
    }
}
