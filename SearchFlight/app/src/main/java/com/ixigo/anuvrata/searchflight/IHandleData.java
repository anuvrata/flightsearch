package com.ixigo.anuvrata.searchflight;

import java.util.ArrayList;

//Interface to be implemented by listener to GetFlightDataTask
public interface IHandleData {
    void showProgressBar();
    void hideProgressBar();
    void setDataInPageWithResult(ArrayList<ICarrier> result);
}
