package com.ixigo.anuvrata.searchflight;

import java.util.ArrayList;
import java.util.concurrent.Callable;

//Interface to support background task
//This design pattern is borrowed from: https://medium.com/swlh/asynctask-is-deprecated-now-what-f30c31362761
public interface CustomCallable extends Callable<ArrayList<ICarrier>> {
    void setDataAfterLoading(ArrayList<ICarrier> result);
    void setUiForLoading();
}
