package com.ixigo.anuvrata.searchflight;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

//Activity to show full details of a flight selected by clicking on the list view
public class ShowFlightFullInfo extends AppCompatActivity {

    private TextView mAirline;
    private TextView mOrigin;
    private TextView mDestination;
    private TextView mDepartureTime;
    private TextView mArrivalTime;
    private TextView mTravelClass;
    private TextView mProvider;
    private TextView mFare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_flight_full_info);
        mAirline = (TextView) findViewById(R.id.airline);
        mOrigin = (TextView) findViewById(R.id.origin);
        mDestination = (TextView) findViewById(R.id.destination);
        mDepartureTime = (TextView) findViewById(R.id.departureTime);
        mArrivalTime = (TextView) findViewById(R.id.arrivalTime);
        mTravelClass = (TextView) findViewById(R.id.travelclass);
        mProvider = (TextView) findViewById(R.id.provider);
        mFare = (TextView) findViewById(R.id.fare);

        mAirline.setText(getIntent().getStringExtra("airline"));
        mOrigin.setText(getIntent().getStringExtra("origin"));
        mDestination.setText(getIntent().getStringExtra("destination"));
        mDepartureTime.setText(getIntent().getStringExtra("departureTime"));
        mArrivalTime.setText(getIntent().getStringExtra("arrivalTime"));
        mTravelClass.setText(getIntent().getStringExtra("travelClass"));
        mProvider.setText(getIntent().getStringExtra("provider"));
        mFare.setText("Rs. " + getIntent().getStringExtra("fare"));
    }
}