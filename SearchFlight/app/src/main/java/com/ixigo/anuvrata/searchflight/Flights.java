package com.ixigo.anuvrata.searchflight;

//Class storing information about flights: it implements ICarrier which is an interface for any
//carrier like flights, buses, railways etc.

public class Flights implements ICarrier {
    private final String mOrigin;
    private final String mDestination;
    private final String mArrivalTime;
    private final String mDepartureTime;
    private final String mFare;
    private final String mProvider;
    private final String mAirline;
    private final String mTravelClass;

    Flights(String origin, String destination, String arrivalTime, String departureTime,
    String fare, String provider, String code, String travelClass)
    {
        mOrigin = origin;
        mDestination = destination;
        mArrivalTime = arrivalTime;
        mDepartureTime = departureTime;
        mFare = fare;
        mProvider = provider;
        mAirline = code;
        mTravelClass = travelClass;
    }

    @Override
    public String getOrigin() {
        return mOrigin;
    }

    @Override
    public String getDestination() {
        return mDestination;
    }


    @Override
    public String getArrivalTime() {
        return mArrivalTime;
    }

    @Override
    public String getDepartureTime() {
        return mDepartureTime;
    }

    @Override
    public String getFare() {
        return mFare;
    }

    @Override
    public String getProvider() {
        return mProvider;
    }

    @Override
    public String getAirline() {
        return mAirline;
    }

    @Override
    public String getTravelClass() {
        return mTravelClass;
    }
}
