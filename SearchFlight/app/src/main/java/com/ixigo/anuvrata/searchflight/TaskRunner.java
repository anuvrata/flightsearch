package com.ixigo.anuvrata.searchflight;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

//Class to run background async task:
//This design is borrowed from https://medium.com/swlh/asynctask-is-deprecated-now-what-f30c31362761
public class TaskRunner {

    private final Handler handler = new Handler(Looper.getMainLooper());
    private final Executor executor = Executors.newCachedThreadPool();

    public void executeAsync(CustomCallable callable) {
        try {
            callable.setUiForLoading();
            executor.execute(new RunnableTask(handler, callable));
        } catch (Exception e) {
            Log.e("Execute Async", e.getMessage());
        }
    }

    public static class RunnableTask implements Runnable{
        private final Handler handler;
        private final CustomCallable callable;

        public RunnableTask(Handler handler, CustomCallable callable) {
            this.handler = handler;
            this.callable = callable;
        }

        @Override
        public void run() {
            try {
                final ArrayList<ICarrier> result = callable.call();
                handler.post(new RunnableTaskForHandler(callable, result));
            } catch (Exception e) {
                Log.e("RunnableTask: run", e.getMessage());
            }
        }
    }

    public static class RunnableTaskForHandler implements Runnable{

        private CustomCallable callable;
        private ArrayList<ICarrier> result;

        public RunnableTaskForHandler(CustomCallable callable, ArrayList<ICarrier> result) {
            this.callable = callable;
            this.result = result;
        }

        @Override
        public void run() {
            callable.setDataAfterLoading(result);
        }
    }
}
